import com.memento.Editor;
import com.memento.TextArea;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Editor editor = new Editor();
        TextArea textArea = new TextArea();
        Scanner input = new Scanner(System.in);
        printInstructions();
        while (true) {
            System.out.println("Enter action:");
            String action = input.nextLine().toLowerCase();
            switch (action) {
                case "write":
                    System.out.println("Enter your text content");
                    String text = input.nextLine();
                    textArea.setText(text);
                    //automatically keeps track of state
                    editor.keepTrackOfState(textArea.takeSnapshot());
                    break;
                case "undo":
                    TextArea.Snapshot undoneState = editor.undo();
                    if (undoneState == null){
                        System.out.println("No history available!");
                        break;
                    }
                    textArea.restore(undoneState);
                    break;
                case "print":
                    textArea.printContent();
                    break;
                case "quit":
                    break;
            }
        }
    }
    public static void printInstructions() {
        System.out.println(
                "\n\t\t\t\t Instructions:" +
                        "\n\t\twrite: gives you the prompt to enter your text" +
                        "\n\t\tundo:  undo the the previous action" +
                        "\n\t\tprint: display document content" +
                        "\n\t\tquit:  Closes the app"
        );
    }
}

