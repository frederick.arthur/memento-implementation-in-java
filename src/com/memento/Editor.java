package com.memento;

import java.util.LinkedList;
import java.util.Deque;

// caretaker
public class Editor {
    private final Deque<TextArea.Snapshot> savedHistory;
    public Editor() {
        this.savedHistory = new LinkedList<>();
    }

    public void keepTrackOfState(TextArea.Snapshot snapshot){
        savedHistory.addFirst(snapshot);
        System.out.println("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
        System.out.println("Saved!");
    }

    public TextArea.Snapshot undo(){
       if (savedHistory.isEmpty()){
          return null;
       }
        return savedHistory.removeFirst();
    }
}