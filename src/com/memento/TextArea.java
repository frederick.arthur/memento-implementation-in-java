package com.memento;

//Originator
public class TextArea {
    private String text;
    public void setText(String textToSave) {
        this.text = textToSave;
    }

    public Snapshot takeSnapshot() {
        return new Snapshot(this.text);
    }

    public void restore(Snapshot memento) {
        this.text = memento.getSavedText();
    }

    public void printContent(){
        System.out.println("Content:");
        System.out.println("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
        System.out.println(this.text);
        System.out.println("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
    }

    //memento
    public  class Snapshot {
        private final String savedText;
        public Snapshot(String textToSave) {
            this.savedText = textToSave;
        }

        private String getSavedText() {
            return savedText;
        }
    }
}
